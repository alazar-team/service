var express = require('express'),
  cors = require('cors'),
  bodyParser = require('body-parser'),
  app = express();

var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/alazar');
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function(callback) {
  console.log('Mongo Conectado!');
});

var Schema = mongoose.Schema({
    response: String,
    geo: Array,
    device: Object
  }),
  Model = mongoose.model('Response', Schema);;

app.get('/response', function(req, res, next) {
  Model.find({}, function(err, response) {
    if (err) return res.json(err, 500);
    res.json(response);
    //next();
  });
});
app.post('/response', cors({
  origin: '*'
}), bodyParser.urlencoded(), function(req, res, next) {
  var model = new Model({
    response: req.body.response,
    geo: JSON.parse(req.body.geo),
    device: JSON.parse(req.body.device)
  });
  model.save(function(err, response) {
    if (err) return res.json(err, 500);
    res.json({
      id: response.id
    });
    //next();
  });
});

app.listen(3000, function() {
  console.log('AlAzar :: Started');
});
